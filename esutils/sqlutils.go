package esutils

func EsSqlToMapSlice(columns []SqlColumn, rows Rows) []map[string]string {
	if len(columns) == 0 || len(rows) == 0 {
		return nil
	}
	cl := len(columns)
	rl := len(rows[0])
	if cl != rl {
		return nil
	}
	values := make([]map[string]string, len(rows))
	for i := 0; i < len(rows); i++ {
		row := rows[i]
		value := make(map[string]string, len(rows))
		for j := 0; j < len(columns); j++ {
			k := columns[j]
			v := row[j]
			value[k.Name] = v
		}
		values[i] = value
	}
	return values
}

func EsSqlToModel[T any](columns []SqlColumn, rows Rows) []map[string]string {
	if len(columns) == 0 || len(rows) == 0 {
		return nil
	}
	cl := len(columns)
	rl := len(rows[0])
	if cl != rl {
		return nil
	}
	values := make([]map[string]string, len(rows))
	for i := 0; i < len(rows); i++ {
		row := rows[i]
		value := make(map[string]string, len(rows))
		for j := 0; j < len(columns); j++ {
			k := columns[j]
			v := row[j]
			value[k.Name] = v
		}
		values[i] = value
	}
	return values
}
