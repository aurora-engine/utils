package esutils

// SqlColumn es sql执行返回的列
type SqlColumn struct {
	Name string `json:"name,omitempty"`
	Type string `json:"type,omitempty"`
}

// Rows es sql 执行返回的行
type Rows [][]string

type SqlResult struct {
	Columns []SqlColumn `json:"columns,omitempty"`
	Rows    Rows        `json:"rows,omitempty"`
}
