package rep

import (
	jsoniter "github.com/json-iterator/go"
	"net/http"
	"strings"
)

// Response 请求处理统一返回结果
type Response struct {
	Code   `json:"code"` // 状态码
	Status int           `json:"status"` // 响应状态码
	Msg    string        `json:"msg"`    // 附加消息
	Data   any           `json:"data"`   // 响应数据
	Err    string        `json:"err"`    // 错误信息
}

// Success 业务处理成功
func Success(data any, message ...string) Response {
	join := strings.Join(message, "")
	return Response{
		Code:   Ok,
		Status: http.StatusOK,
		Msg:    join,
		Data:   data,
	}
}

// Fail 业务处理失败
func Fail(e error, message ...string) Response {
	join := strings.Join(message, "")
	err := ""
	if e != nil {
		err = e.Error()
	}
	return Response{
		Code:   Err,
		Status: http.StatusInternalServerError,
		Msg:    join,
		Err:    err,
	}
}

// Error 错误响应
func Error(err error, message ...string) Response {
	e := ""
	if err != nil {
		e = err.Error()
	}
	return Response{
		Code:   Err,
		Status: http.StatusInternalServerError,
		Msg:    strings.Join(message, ""),
		Err:    e,
	}
}

// Unknown 未知操作
func Unknown() Response {
	return Response{
		Code:   "unknown",
		Status: http.StatusBadRequest,
		Msg:    "unknown",
		Data:   nil,
		Err:    "unknown",
	}
}

func (msg *Response) String() string {
	marshal, err := jsoniter.Marshal(msg)
	if err != nil {
		return ""
	}
	return string(marshal)
}

// Page 分页数据
type Page struct {
	Count int64 `json:"count"` //总数
	Rows  any   `json:"rows"`  //数据内容
}

func NewPage(count int64, data any) *Page {
	return &Page{
		Count: count,
		Rows:  data,
	}
}
