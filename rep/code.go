package rep

type Code string

const (
	Err = "9999" // 业务失败
	Ok  = "0000" // 业务成功
)
