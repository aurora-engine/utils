package datautils

import (
	"reflect"
	"strings"
)

// ToMap 把 map 或者 结构体完全转化为 map[any]
func ToMap(value any) map[string]any {
	if value == nil {
		return nil
	}
	valueOf := reflect.ValueOf(value)
	if valueOf.Kind() != reflect.Map && valueOf.Kind() != reflect.Struct && valueOf.Kind() != reflect.Pointer {
		return map[string]any{}
	}
	if valueOf.Kind() == reflect.Pointer {
		valueOf = valueOf.Elem()
		return ToMap(valueOf.Interface())
	}
	ctx := make(map[string]any)
	switch valueOf.Kind() {
	case reflect.Struct:
		structToMap(valueOf, ctx)
	case reflect.Map:
		mapToMap(valueOf, ctx)
	}
	return ctx
}

func structToMap(value reflect.Value, ctx map[string]any) {
	for i := 0; i < value.NumField(); i++ {
		field := value.Field(i)
		if !value.Type().Field(i).IsExported() {
			continue
		}
		FiledType := value.Type().Field(i)
		key := FiledType.Name
		if tag, b := FiledType.Tag.Lookup("name"); b && tag != "" {
			key = tag
		}
		key = strings.ToLower(key)
		v := field.Interface()
		if dataType(v) {
			continue
		}
		if field.Kind() == reflect.Slice {
			v = filedToMap(v)
		}
		if field.Kind() == reflect.Struct || field.Kind() == reflect.Pointer || field.Kind() == reflect.Map {
			v = ToMap(v)
		}
		ctx[key] = v
	}
}

func mapToMap(value reflect.Value, ctx map[string]any) {
	mapIter := value.MapRange()
	for mapIter.Next() {
		key := mapIter.Key().Interface().(string)
		vOf := mapIter.Value()
		v := vOf.Interface()
		if vOf.Kind() == reflect.Interface {
			if vOf.Elem().Kind() == reflect.Slice {
				if vOf.Elem().Type().Elem().Kind() == reflect.Struct || vOf.Elem().Type().Elem().Kind() == reflect.Pointer || vOf.Elem().Type().Elem().Kind() == reflect.Map {
					v = filedToMap(v)
				}
			}
			if vOf.Elem().Kind() == reflect.Struct || vOf.Elem().Kind() == reflect.Map || vOf.Elem().Kind() == reflect.Pointer {
				v = ToMap(v)
			}
		}
		if dataType(v) {
			continue
		}
		if vOf.Kind() == reflect.Slice {
			v = filedToMap(v)
		}
		if vOf.Kind() == reflect.Struct || vOf.Kind() == reflect.Map || vOf.Kind() == reflect.Pointer {
			v = ToMap(v)
		}
		ctx[key] = v
	}
}

func filedToMap(value any) []map[string]any {
	valueOf := reflect.ValueOf(value)
	elem := valueOf.Type().Elem()
	arr := make([]map[string]any, 0)
	length := valueOf.Len()
	switch elem.Kind() {
	case reflect.Struct, reflect.Pointer:
		for i := 0; i < length; i++ {
			val := valueOf.Index(i)
			m := ToMap(val.Interface())
			arr = append(arr, m)
		}
	case reflect.Map:
		for i := 0; i < length; i++ {
			val := valueOf.Index(i)
			iter := val.MapRange()
			m := map[string]any{}
			for iter.Next() {
				key := iter.Key().Interface().(string)
				v := iter.Value()
				var vals any
				vals = v.Interface()
				if v.Kind() == reflect.Slice {
					vals = filedToMap(v.Interface())
				}
				if v.Kind() == reflect.Struct || v.Kind() == reflect.Pointer || v.Kind() == reflect.Map {
					vals = ToMap(v.Interface())
				}
				m[key] = vals
			}
			arr = append(arr, m)
		}
	}
	return arr
}

func dataType(value any) bool {

	return false
}
