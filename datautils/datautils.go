package datautils

import (
	"encoding/json"
	"errors"
	"reflect"
	"slices"
	"strconv"
)

// Get 基础数据类型转化。把value转化为指定的数据类型T
func Get[T any](value any) (v T, err error) {
	return get[T](value)
}

func get[T any](value any) (v T, err error) {
	defer func(v error) {
		if e := recover(); e != nil {
			switch t := e.(type) {
			case string:
				err = errors.New(t)
			case error:
				err = t
			default:
				marshal, _ := json.Marshal(e)
				err = errors.New(string(marshal))
			}
		}
	}(err)
	valueOf := reflect.ValueOf(&v).Elem()
	var data any = v
	switch element := value.(type) {
	case int:
		switch data.(type) {
		case string:
			s := strconv.Itoa(element)
			valueOf.SetString(s)
		case float32, float64:
			f := float64(element)
			valueOf.SetFloat(f)
		case bool:
			flag := element != 0
			valueOf.SetBool(flag)
		default:
			valueOf.SetInt(int64(element))
		}
	case int32:
		switch data.(type) {
		case string:
			s := strconv.FormatInt(int64(element), 10)
			valueOf.SetString(s)
		case float32, float64:
			f := float64(element)
			valueOf.SetFloat(f)
		case bool:
			flag := element != 0
			valueOf.SetBool(flag)
		default:
			valueOf.SetInt(int64(element))
		}
	case int64:
		switch data.(type) {
		case string:
			s := strconv.FormatInt(element, 10)
			valueOf.SetString(s)
		case float32, float64:
			f := float64(element)
			valueOf.SetFloat(f)
		case bool:
			flag := element != 0
			valueOf.SetBool(flag)
		default:
			valueOf.SetInt(element)
		}
	case float32:
		switch data.(type) {
		case int, int32, int64:
			i := int64(element)
			valueOf.SetInt(i)
		case string:
			s := strconv.FormatFloat(float64(element), 'f', -1, 32)
			valueOf.SetString(s)
		case bool:
			flag := element != 0
			valueOf.SetBool(flag)
		default:
			valueOf.SetFloat(float64(element))
		}
	case float64:
		switch data.(type) {
		case int, int32, int64:
			i := int64(element)
			valueOf.SetInt(i)
		case string:
			s := strconv.FormatFloat(element, 'f', -1, 64)
			valueOf.SetString(s)
		case bool:
			flag := element != 0
			valueOf.SetBool(flag)
		default:
			valueOf.SetFloat(element)
		}
	case string:
		switch data.(type) {
		case int, int32, int64:
			var i int64
			i, err = strconv.ParseInt(element, 0, 64)
			valueOf.SetInt(i)
		case bool:
			var flag bool
			flag, err = strconv.ParseBool(element)
			valueOf.SetBool(flag)
		}
	case bool:
		switch data.(type) {
		case int, int64, int32:
			var i int64
			if element {
				i = 1
			}
			valueOf.SetInt(i)
		case float32, float64:
			var f float64
			if element {
				f = 1
			}
			valueOf.SetFloat(f)
		case string:
			s := strconv.FormatBool(element)
			valueOf.SetString(s)
		}
	default:
		valueOf.Set(reflect.ValueOf(value))
	}
	return
}

// JsonGo json反序列化
func JsonGo[T any](value any) (T, error) {
	return jsongo[T](value)
}

func jsongo[T any](value any) (T, error) {
	var data T
	var buf []byte
	switch v := value.(type) {
	case string:
		buf = []byte(v)
	case []byte:
		buf = v
	}
	err := json.Unmarshal(buf, &data)
	if err != nil {
		return data, err
	}
	return data, nil
}

func Copy(source, target any, fields ...string) {
	copyStruct(source, target, fields...)
}

func copyStruct(source, target any, fields ...string) {
	if source == nil {
		panic("source is nil")
	}
	sourceValue := reflect.ValueOf(source)
	targetValue := reflect.ValueOf(target)
	// 传递的目标 为nil 我们就创建它
	if target == nil {
		elem := reflect.New(sourceValue.Type()).Elem()
		targetValue.Set(elem)
	}
	if sourceValue.Kind() != reflect.Pointer && targetValue.Kind() != reflect.Pointer {
		panic("source and target must be pointer")
	}
	if sourceValue.Kind() == reflect.Pointer && targetValue.Kind() == reflect.Pointer {
		sourceValue = sourceValue.Elem()
		targetValue = targetValue.Elem()
		if sourceValue.Kind() != reflect.Struct && targetValue.Kind() != reflect.Struct {
			panic("source and target must be struct")
		}
	}
	for i := 0; i < sourceValue.NumField(); i++ {
		sourcefield := sourceValue.Field(i)
		name := sourceValue.Type().Field(i).Name
		// 过滤字段
		if slices.Contains(fields, name) {
			continue
		}
		var field reflect.StructField
		var b bool
		if field, b = targetValue.Type().FieldByName(name); !b {
			continue
		}
		tagetfield := targetValue.FieldByName(name)
		if field.IsExported() && sourcefield.Type().AssignableTo(tagetfield.Type()) {
			// 进行深拷贝
			switch sourcefield.Kind() {
			case reflect.Pointer:
			case reflect.Struct:
			case reflect.Map:
			case reflect.Slice:
			case reflect.Chan:
			}
			tagetfield.Set(sourcefield)
		}
	}
}
