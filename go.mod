module gitee.com/aurora-engine/utils

go 1.20

require (
	github.com/json-iterator/go v1.1.12
	github.com/sirupsen/logrus v1.9.3
	go.uber.org/zap v1.25.0
	golang.org/x/term v0.11.0
)

require (
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/sys v0.11.0 // indirect
)
