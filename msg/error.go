package msg

type baseError struct {
	err string
}

func (e *baseError) Error() string {
	return e.err
}
