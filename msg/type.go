package msg

type Message[T any] interface {
	GetStatus() int
	GetMsg() string
	GetData() T
	GetError() error

	SetData(T)
	SetError(err error)
	SetMsg(string)
	SetStatus(int)
}
